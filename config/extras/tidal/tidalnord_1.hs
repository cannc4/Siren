let mcc = midicmd "control"
    nchan val = midichan (val - 1)
    nvar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 4
    nkvol ccval = ccn 12 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnvol ccval = ccn 13 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatvol ccval = ccn 14 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercvol ccval = ccn 15 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercfilter ccval = ccn 21 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercres ccval = ccn 28 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercosc1 ccval = ccn 22 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercosc2 ccval = ccn 23 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercosc3 ccval = ccn 24 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nperchold ccval = ccn 29 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercatk ccval = ccn 30 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercdec ccval = ccn 31 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnosc ccval = ccn 33 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnatk ccval = ccn 27 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnenv ccval = ccn 34 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsndec ccval = ccn 35 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnfilter ccval = ccn 36 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnres ccval = ccn 37 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatosc ccval = ccn 38 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatcol ccval = ccn 26 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatatk ccval = ccn 39 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatenv ccval = ccn 40 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatdec ccval = ccn 41 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatfilter ccval = ccn 42 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatres ccval = ccn 43 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkosc ccval = ccn 44 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkshp ccval = ccn 45 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkpch ccval = ccn 46 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkatk ccval = ccn 47 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkenv ccval = ccn 48 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkmod ccval = ccn 49 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshape ccval = ccn 51 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshapefilter ccval = ccn 50 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshaperes ccval = ccn 52 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshenv ccval = ccn 53 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ntone ccval = ccn 54 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ncutoff ccval = ccn 55 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nreso ccval = ccn 56 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ndist ccval = ccn 57 # ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ndelvar ccval = ccn 70 #ccv (ccval * 15) #midichan 5 #mcc 
    ndellofi ccval = ccn 12 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelsat ccval = ccn 13 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndeldry ccval = ccn 14 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelwet ccval = ccn 15 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelsens ccval = ccn 16 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndeltime ccval = ccn 21 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfb ccval = ccn 22 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfilter ccval = ccn 23 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfbshape ccval = ccn 19 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfbrate ccval = ccn 20 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelwet ccval = ccn 24 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelsp1 ccval = ccn 25 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfb1 ccval = ccn 26 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelsp2 ccval = ccn 27 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    ndelfb2 ccval = ccn 28 #ccv (scale 0 127 $ ccval) #midichan 5 #mcc
    nbvar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 6
    nb1 ccval = ccn 12 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nb2 ccval = ccn 13 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nb1filter ccval = ccn 14 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nb1res ccval = ccn 15 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nb2filter ccval = ccn 16 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nb2res ccval = ccn 19 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nvoicevar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 7
    nvoicefilter ccval = ccn 12 #ccv (scale 0 127 $ ccval) #mcc #midichan 7
    nvoiceres ccval = ccn 13 #ccv (scale 0 127 $ ccval) #mcc #midichan 7