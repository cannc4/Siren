import React from 'react';
import { observer, inject } from 'mobx-react';

import '../styles/App.css';
import '../styles/Home.css';
import '../styles/Layout.css';

import MIDIKnobs from './MIDIKnobs';
import OrbitKnobs from './OrbitKnobs';

@inject('controllerStore')
@observer
export default class Controller extends React.Component {

  render() {
    console.log("RENDER Controller.js");    
    return (<div className={'Controller'}>
      <MIDIKnobs/>
    </div>
    )
  }
}      
