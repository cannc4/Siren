import React from 'react';
import Knob from 'react-canvas-knob';
import { inject, observer } from 'mobx-react';
import _ from 'lodash'

@inject('controllerStore')
@observer
export default class OrbitKnobs extends React.Component {
    
    changeSpeed = (val) => {

     this.props.controllerStore.changeSpeed(val)

    }

    changeLPF = (val) => {

      this.props.controllerStore.changeLPF(val)

    }

    saveOrbitParams = () =>{
    //this.props.controllerStore.saveOrbitParams();

    }
    render() {

    let ctx = this;
    
    let orbitKnobs = this.props.controllerStore.OrbitParams;
    return (
        <div className= {"draggableCancel"}>
            <input className={'Input draggableCancel'} title={"Orbit no"} placeholder={0} onChange={(e) => {this.props.controllerStore.updateOrbit(_.toInteger(e.target.value))}}/>
            <div className= {'MIDIKnobs'}key = {"orbit_0_speed"}>
                {orbitKnobs[0] !== undefined &&
                     <div className={
                        'MIDIKnobsColumn' 
                    } key = {"0_" + 1}>
                    
                            <div className = {'KnobText'}>
                                {orbitKnobs[0].name}
                            </div>
                            <Knob 
                            title ={orbitKnobs[0].name}
                            className = {'MIDIKnobsRow'}
                            height ={75}
                            displayInput = {false}
                            width ={75}
                            min = {-4}
                            max = {4}
                            step = {0.05}
                            font= {'Inconsolata'}
                            value={orbitKnobs[0].ccv}
                            onChange={ctx.changeSpeed}/>
                    </div>
                }
            </div>
            <div className= {'MIDIKnobs'}key = {"orbit_0_lpf"}>
                {orbitKnobs[1] !== undefined &&
                     <div className={
                        'MIDIKnobsColumn' 
                    } key = {"0_" + 1}>
                    
                            <div className = {'KnobText'}>
                                {orbitKnobs[1].name}
                            </div>
                            <Knob 
                            title ={orbitKnobs[1].name}
                            className = {'MIDIKnobsRow'}
                            height ={75}
                            displayInput = {false}
                            width ={75}
                            max = {13000}
                            font= {'Inconsolata'}
                            value={orbitKnobs[1].ccv}
                            onChange={ctx.changeLPF}/>
                    </div>
                }
            </div>
        </div>    
    )}
}


 // <div>
        //     <input className={'Input draggableCancel'} title={"Orbit no"} placeholder={0} onChange={(e) => {this.props.controllerStore.updateOrbit(_.toInteger(e.target.value))}}/>
        //     <div className= {'MIDIKnobs'}key = {"orbit_0_"}>
        //         {orbitKnobs !== undefined && orbitKnobs.map((orbitParam, i) => {
        //             return <div className={
        //                 'MIDIKnobsColumn' 
        //             } key = {"0_" + i}>
                    
        //                     <div className = {'KnobText'}>
        //                         {orbitParam.name}
        //                     </div>
        //                     <Knob 
        //                     title ={orbitParam.name}
        //                     className = {'MIDIKnobsRow'}
        //                     height ={75}
        //                     displayInput = {false}
        //                     width ={75}
        //                     max = {10}
        //                     font= {'Inconsolata'}
        //                     value={orbitParam.ccv}
        //                     onChange={ctx.changeValue}/>
        //             </div>
        //         })}
        //     </div>
        // </div>    