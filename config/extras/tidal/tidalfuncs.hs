
import Sound.Tidal.Chords
import Sound.Tidal.Scales
import Sound.Tidal.Utils
import Data.Maybe (fromMaybe, maybe, isJust, fromJust)
import Control.Applicative
import Data.Maybe
import qualified Data.Map.Strict as Map

let cap a b p = within (0.25, 0.75) (slow 2 . rev . stut 8 a b) p
    cap3 a b p = within (0.66, 0.97) (hurry 0.5 . rev . stut 8 a b) p
    toggle t f p = if (1 == t) then f $ p else id $ p
    toggles p xs = unwrap $ (xs !!!) <$> p
    mute x = filterValues (x /=)
    mutelist xs = filterValues (\x -> notElem x xs)
    capj a b p = within (0.5, 0.75) (juxBy 0.35 (rev) . stut 8 a b) p
    capf a b p = within (0.75, 0.95) (rev . stut 4 a b) p
    capx a b c d e p = within (a, b) (slow 2 . rev . stut c d e) p
    capz a b p = within (0.5, 0.85) (degradeBy 0.5 . iter 3 . stut 4 a b) p
    layer fs p = stack $ map ($ p) fs
    foldpxx a f p = superimpose (((a/2 + a*2) ~>) . f) $ superimpose (((a + a/2) ~>) . f) $ p
    foldpx a p = foldpxx a ((|*| gain "0.7") . (|=| end "0.2") . (|=| speed "0.725")) p
    foldp p = foldpx 0.125 p
    foldcxx a f p = off 0.5 (((a/2 + a*2) ~>) . f) $ superimpose (((a + a/2) ~>) . f) $ p
    foldcx a p = foldcxx a ((|*| end "0.2") . (|=| begin "0.05") . (# coarse "2")) p
    foldc p = foldcx 0.33 p
    mfold p = foldEvery [3,5] (0.25 <~) $ p
    shift x p = (x <~) p
    ccn = ctlNum
    ccv = control
    mcmd = midicmd "[noteOn, control]"
    majork = ["major", "minor", "minor", "major", "major", "minor", "dim7"]
    minork = ["minor", "minor", "major", "minor", "major", "major", "major"]
    doriank = ["minor", "minor", "major", "major", "minor", "dim7", "major"]
    phrygiank = ["minor", "major", "major", "minor", "dim7", "major", "minor"]
    lydiank = ["major", "major", "minor", "dim7", "major", "minor", "minor"]
    mixolydiank = ["major", "minor", "dim7", "major", "minor", "minor", "major"]
    locriank = ["dim7", "major", "minor", "minor", "major", "major", "minor"]
    keyTable = [("major", majork),("minor", minork),("dorian", doriank),("phrygian", phrygiank),("lydian", lydiank),("mixolydian", mixolydiank),("locrian", locriank),("ionian", majork),("aeolian", minork)]
    keyL p = (\name -> fromMaybe [] $ lookup name keyTable) <$> p
    harmonise ch p = scaleP ch p + chord (flip (!!!) <$> p <*> keyL ch)
    scaleUP c p = fromIntegral <$> scaleP c p
    listToChord = stack . map atom
    contToPat  n p = round <$> discretise n p
    contToPat' a b = round <$> struct a b
    c2p  = contToPat
    c2p' = contToPat'
    tremolo' r d = tremolorate r # tremolodepth d
    phaser'  r d = phaserrate  r # phaserdepth  d
    saturate amount = superimpose ((# shape amount).(# hcutoff 6500).(|*| gain 0.9))
    disc = discretise
    modNotec note f p = stack [f $ filterValues (isEq note . (Map.!?note_p)) p,filterValues (not . isEq note . (Map.!?note_p)) p] where isEq a m = (isJust m) && (a == (fromJust $ fromV $ fromJust m))
    modNoten = modNotec
    modN = modNotec
    modNote n f p = stack [f $ filterValues (isEq n . (Map.!?n_p)) p,filterValues (not . isEq n . (Map.!?n_p)) p] where isEq a m = (isJust m) && (a == (fromJust $ fromV $ fromJust m))
    modSource s f p = stack [f $ filterValues (isEq s . (Map.!?s_p)) p,filterValues (not . isEq s . (Map.!?s_p)) p] where isEq a m = (isJust m) && (a == (fromJust $ fromV $ fromJust m))
    sl0 p = modNote 0 (#gain 0) p
    sl1 p = modNote 1 (#gain 0) p
    sl2 p = modNote 2 (#gain 0) p
    sl3 p = modNote 3 (#gain 0) p
    sl4 p = modNote 4 (#gain 0) p
    sl5 p = modNote 5 (#gain 0) p
    sl6 p = modNote 6 (#gain 0) p
    modValuesF par test f p = stack [ f $ filterValues (test' . (Map.!?par)) p, filterValues (not . test' . (Map.!?par)) p ] where test' m = (isJust m) && (test (fvalue $ fromJust m))
    filler p = every 3 (within (0.0, 0.50)(rev )) $ someCyclesBy 0.125 (within (0.75, 1.0)(trunc 0.5)) p
    vou n p =  inside 3(within (0.75,0.99)(slow 4 . cap n 0.33)) p
    startclock d p = do {now <- getNow; d $ (pure (nextSam now)) ~> p}
    oneshot d p = startclock d $ seqP [(0, 1, p)]
    twoshot d p = startclock d $ seqP [(0, 2, p)]
    pff d p = off d ((#speed "0.25?").(#gain 0.7).(#legato 1)) p
    wchoose weights values = choose $ concatMap (\x -> replicate (fst x) (snd x)) (zip weights values)

    --dip p =  every 6 (density 4. slow 4) $ whenmod 16 10 ( ifp ((== 0).(flip mod 5))(stut 8 0.25  0.75)(# coarse "6 16 20 27"))  $ sometimes (|*| speed "[0.9 0.8 1.15, 1 0.7 1.0]") p
    --ladder a b p =  when ((elem a).show)(rev . striate b) p -- EX: $ ladder '2' 16
