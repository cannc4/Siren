import React from 'react';
import Knob from 'react-canvas-knob';
import _ from 'lodash'
import { inject, observer } from 'mobx-react';
import { executionCssByEvent } from '../keyFunctions';
@inject('controllerStore')
@observer
export default class MIDIKnobs extends React.Component {
    
  changeValue = (name,val) => {

      if(name !== undefined)
        this.props.controllerStore.modulateParam(name,val);
  }

  saveMIDI = () =>{
    this.props.controllerStore.saveParams();

  }

  handlePresetClick = ( index, name,event) => {

    if(event.altKey) {
        this.props.controllerStore.deletePreset(name)
    }else if(event.shiftKey){
        this.props.controllerStore.overwritePreset(name)
    }
    else{
        this.props.controllerStore.changePreset(name)
    }
  }
  render() {

    let ctx = this;
    
    let knobChunks = _.chunk(this.props.controllerStore.MIDIParams, 8)
    return (
        <div className={"draggableCancel"}>
            <input type="String" id= {'nord_perf'}
                  onKeyUp={(event) => {
                    if(event.ctrlKey && event.keyCode === 13){
                        executionCssByEvent(event);
                        this.props.controllerStore.updatePerf(event.target.value)
                        this.props.controllerStore.compilePerf();
                        }
                    }}
                  placeholder={"Nord G2 Performance"} 
                  value={this.props.controllerStore.getPerf}
                  className = {"GText"}
                  onChange={(event) => {
                      this.props.controllerStore.updatePerf(event.target.value)
                  }}
                  />
            <button className={'Button draggableCancelNested'} 
                onClick={() => {ctx.props.controllerStore.getControllerSnapshot()}}>{'Load Snapshot'} 
            </button>
{/*           
            <input type="String" id= {'nord_name'}
                  placeholder={"Preset name"}
                  value={this.props.controllerStore.getPreset}
                  className = {"GText"}
                  onChange={(event) => {
                      ctx.props.controllerStore.updatePresetName(event.target.value)
                  }}
                  /> */}
            {/* <button className={'Button draggableCancelNested'} 
                    onClick={() => {ctx.saveMIDI()}}>{'Save Preset'} 
            </button> */}
            {this.props.controllerStore.MIDIPresets.map((preset, i ) => {
                return(
                    <button key = {i} className={'Button draggableCancelNested'} 
                        onClick= {ctx.handlePresetClick.bind(ctx,i,preset.presetName)}>
                        {preset.presetName? preset.presetName : null }
                    </button>  
                )
            })}
            

            <div className= {'MIDIKnobs'}key = {"cont_0_"}>
                {knobChunks[0]!== undefined && knobChunks[0].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"0_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                            cursor = {false}
                            thickness = {0.6}
                            bgColor = {'#959595'}
                            stopper = {true}
                            displayInput = {false}
                            disableMouseWheel = {true}
                            title ={MIDIParam.name ? MIDIParam.name : "NA"}
                            className = {'MIDIKnobsRow'}
                            height ={72}
                            width ={72}
                            max = {128}
                            font= {'Inconsolata'}
                            value={MIDIParam.ccv}
                            readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>
                })}
            </div>
            <div className= {'MIDIKnobs'}key = {"cont_1_"}>
                {knobChunks[1] !== undefined && knobChunks[1].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"1_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                            cursor = {false}
                            thickness = {0.6}
                            bgColor = {'#959595'}
                            stopper = {true}
                            displayInput = {false}
                            disableMouseWheel = {true}
                            title ={MIDIParam.name ? MIDIParam.name : "NA"}
                            className = {'MIDIKnobsRow'}
                            height ={72}
                            width ={72}
                            max = {128}
                            font= {'Inconsolata'}
                            value={MIDIParam.ccv}
                            readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}

            </div>
            <div className= {'MIDIKnobs'}key = {"cont_2_"}>
                {knobChunks[2] !== undefined && knobChunks[2].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"2_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}

            </div>
            <div className= {'MIDIKnobs'} key = {"cont_3_"}>
                {knobChunks[3] !== undefined && knobChunks[3].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"3_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}

            </div>
            <div className= {'MIDIKnobs'} key = {"cont_4_"}>
                {knobChunks[4] !== undefined && knobChunks[4].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"4_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}


            </div>
            <div className= {'MIDIKnobs'} key = {"cont_5_"}>
                {knobChunks[5] !== undefined && knobChunks[5].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"5_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>
            <div className= {'MIDIKnobs'} key = {"cont_6_"}>
                {knobChunks[6] !== undefined && knobChunks[6].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"6_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>
            <div className= {'MIDIKnobs'} key = {"cont_7_"}>
                {knobChunks[7] !== undefined && knobChunks[7].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"7_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>    
          <div className= {'MIDIKnobs'} key = {"cont_8_"}>
                {knobChunks[8] !== undefined && knobChunks[8].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"8_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>    
          <div className= {'MIDIKnobs'} key = {"cont_9_"}>
                {knobChunks[9] !== undefined && knobChunks[9].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"9_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>    
          <div className= {'MIDIKnobs'} key = {"cont_10_"}>
                {knobChunks[10] !== undefined && knobChunks[10].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"10_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>
          <div className= {'MIDIKnobs'} key = {"cont_11_"}>
                {knobChunks[11] !== undefined && knobChunks[11].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"11_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>    
          <div className= {'MIDIKnobs'} key = {"cont_12_"}>
                {knobChunks[12] !== undefined && knobChunks[12].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"12_" + i}>
                    
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>   
          <div className= {'MIDIKnobs'} key = {"cont_13_"}>
          
                {knobChunks[13] !== undefined && knobChunks[13].map((MIDIParam, i) => {
                    return <div className={
                        'MIDIKnobsColumn' 
                    } key = {"13_" + i}>
                        
                            <div className = {'KnobText'}>
                                {MIDIParam.name ? MIDIParam.name : "NA"}
                            </div>
                            <Knob 
                           cursor = {false}
                           thickness = {0.6}
                           bgColor = {'#959595'}
                           stopper = {true}
                           displayInput = {false}
                           disableMouseWheel = {true}
                           title ={MIDIParam.name ? MIDIParam.name : "NA"}
                           className = {'MIDIKnobsRow'}
                           height ={72}
                           width ={72}
                           max = {128}
                           font= {'Inconsolata'}
                           value={MIDIParam.ccv}
                           readOnly = {false}
                            onChange={ctx.changeValue.bind(ctx,MIDIParam.name)}/>
                    </div>   
                })}
          </div>         
        </div>    
    )}
}