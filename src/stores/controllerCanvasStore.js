import {
    observable,
    action,
    computed
} from 'mobx';
import request from '../utils/request';
import io from 'socket.io-client';
import _ from 'lodash';

class controllerCanvasStore {

    sc_log_socket = io('http://localhost:4002/');
    snapshot_socket = io('http://localhost:4040/');
    @observable value = 20;
    @observable scmsg;
    @observable activeNordModularPerf = 'tidalnord_4';
    @observable activeOrbitIndex;
    @observable TidalParams = [{}]
    @observable TidalPresets = [{}]
    @observable activePreset = 'init'
    @observable presetName;
    controllerSnapshot = []
    @observable OrbitParams = [
        {
        name: 'speed',
        t_param_value: 0
        },
        {
        name: 'lpf',
        t_param_value: 10000
        },
        {
        name: 'coarse',
        t_param_value: 0
        },
        {
        name: 'dub',
        t_param_value: 0
        },
    ]

    
    constructor(){
        this.load()
        this.loadParams()
        let ctx = this;    
        this.sc_log_socket.on("/sclog", action((data) => {

            
            ctx.sc = data.trigger;
            let keys = Object.keys(data.trigger);
            
            for(let i = 0; i <keys.length; i++){
            
                if(keys[i]  === 'ctlNum' ){
                    for(let j = 0; j <ctx.TidalParams.length; j++){

                        let pval = parseInt(data.trigger['control']);

                        if( ctx.TidalParams[j].orbit === data.trigger ['orbit'] && ctx.TidalParams[j].t_param_name === data.trigger ['ctlNum']) {

                            ctx.TidalParams[j].t_param_value = pval;

                        }
                    }
                }
            } 
        }))
    }

    @action updateParams(data){
        console.log(data.msg.channel)
        _.forEach(this.TidalParams, function(p) {
            if(data.msg.channel === parseInt(p.orbit) && data.msg.controller === p.t_param_name ){
                p.t_param_value = data.msg.value
            }
        })
    }

    @action
    updatePerf(perf){
        this.activeNordModularPerf = perf;
        
    }
    @action
    updatePresetName(name){
        this.activePreset= name
    }
    @action
    compilePerf(){
        this.load();
        let perf = `:script /Users/canince/Documents/git/Siren/config/extras/tidal/` + this.activeNordModularPerf.toString() + '.hs' 
        this.submitGHC(perf);
    }

    @computed get activePerf (){
        return this.activeNordModularPerf;
    }

    @computed get getActivePreset (){
        return this.activePreset;
    }

    @computed get getActiveOrbitIndex(){
        return this.activeOrbitIndex;
    }

    @action updateOrbit(orbit){
        this.activeOrbitIndex = orbit;
    }
    
    // @action changeSpeed(speed){
    //     this.OrbitParams[0].t_param_value = speed;
    //     let scChan = this.getActiveOrbitIndex + 1;
    //     let expr = "~d"+scChan+".set(\\speed," + speed + ");"
    //     this.submitSC(expr);

    // }

    // @action changeLPF(lpf){
    //     this.OrbitParams[1].t_param_value = lpf;
    //     let scChan = this.getActiveOrbitIndex + 1;
    //     let expr = "~d"+scChan+".set(\\lpf," + lpf + ");"
    //     this.submitSC(expr);
    // }


    submitSC(expression) {
        request.post('http://localhost:3001/console_sc', {
            'pattern': expression
          })
          .then((response) => {
            console.log("RESPONSE SC", response);
          }).catch(function (error) {
            console.error("ERROR", error);
          });
      }

    
      submitGHC(expression) {
        request.post('http://localhost:3001/console_ghc', {
            'pattern': expression
          })
          .then((response) => {
            console.log("RESPONSE GHC");
          }).catch(function (error) {
            console.error("ERROR", error);
          });
      }

    @action
    modulateParam(name,val){
        let ctx = this;
        _.forEach(this.TidalParams, function(p) {
            if(p.name === name.toString()){
                p.t_param_value = val;
                ctx.sendModulation(p.t_param_name, p.t_param_value, p.orbit)
            }
        });        
    }
    
    @action 
    sendOSCModulation(t_param_name, t_param_value, orbit) { 
        //get cc number here
        let OSCObj = {t_param_name :  t_param_name , t_param_value :  parseInt(t_param_value) , orbit: parseInt(orbit)}
        request.post('http://localhost:3001/S_OSC', {
            'OSCObj': OSCObj
        })
        .then(action((response) => {
            if (response.status === 200) {
                console.log(response.status);
                
            } else {
                console.log("OSC failed", response.status);
            }
        })).catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }));
    }
    
    @action 
    sendMIDIModulation(t_param_name,t_param_value, orbit) { 
        //get cc number here
        let MIDIObj = {t_param_name :  t_param_name , t_param_value :  parseInt(t_param_value) , orbit: parseInt(orbit)}
        request.post('http://localhost:3001/MIDI', {
            'MIDIObj': MIDIObj
        })
        .then(action((response) => {
            if (response.status === 200) {
                console.log(response.status);
                
            } else {
                console.log("MIDI failed", response.status);
            }
        })).catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }));
    }
    
    load() {
        request.post('http://localhost:3001/TidalParams', {
            'activeNordModularPerf' : this.activeNordModularPerf
        })
        .then(action((response) => {
            if (response.data.TIdalObjArray) {
            this.TidalParams = _.drop(response.data.TIdalObjArray,2);
            console.log(" ## TidalParams loaded: ", response);
            }
        })).catch(function (error) {
            console.error(" ## TidalParams errors: ", error);
        });
    };

    @action
    changePreset(name){
        let ctx = this;
        console.log("change preset")
       let pobj = this.TidalPresets.find((o, i) => {
            
            if (o.presetName === name) {
                this.TidalParams = o.TidalParams
                this.activePreset = o.presetName 
                return true; // stop searching
            }  
        })
        if(pobj){
            _.forEach(this.TidalParams, function(p) {
                ctx.sendModulation(p.t_param_name, p.t_param_value, p.orbit)
              
            })
        }  
    }

    @action
    deletePreset(name){
        console.log(name)
        let newTidalArray = _.filter(this.TidalPresets, function(o) {
            return o.presetName !== name
        });
        this.TidalPresets = newTidalArray
    }


    @action
    overwritePreset(name){
        let tidalObj= this.TidalPresets ;
        
        let k = this.TidalPresets.find((o, i) => {
            
            if (o.presetName === name) {
                tidalObj[i] = this.TidalParams
                return true; // stop searching
            }  
        })
        if(k){
            this.TidalPresets = tidalObj
        } 
    }

    @action
    loadParams(){
        request.get('http://localhost:3001/MIDILoad')
        .then(action((response) => {
            if (response.data.TidalPresets !== undefined) {
                this.TidalPresets = response.data.TidalPresets;
                console.log(" ## MIDI PRESETS loaded: ", this.TidalPresets);
            }
        })).catch(function (error) {
            console.error(" ## GlobalStore errors: ", error);
        });
    }
    @action
    saveParams() {
        console.log("SAVING..");
        //TODO check for various validations
        let preset = {'presetName': this.getActivePreset, 'TidalParams' :this.TidalParams};
        this.TidalPresets.push(preset)
    
        
        request.post('http://localhost:3001/MIDISave', {
            'TidalPresets' : this.TidalPresets
        })
        .then(action((response) => {
            if (response.status === 200 && response.data !== undefined) {
                console.log("MIDI PRESETS SAVED..")
            }
        })).catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }));
    }



}
export default new controllerCanvasStore();