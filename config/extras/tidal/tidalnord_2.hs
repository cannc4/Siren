let mcc = midicmd "control"
    nchan val = midichan (val - 1)
    nvar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 4
    npercfilter ccval = ccn 21 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercosc1 ccval = ccn 22 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercosc2 ccval = ccn 23 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercfm1 ccval = ccn 24 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercfm2 ccval = ccn 25 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nperclfo1 ccval = ccn 26 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nperclfo2 ccval = ccn 27 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercres ccval = ccn 28 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercenv ccval = ccn 29 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercatk ccval = ccn 30 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercdec ccval = ccn 31 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nperclen ccval = ccn 81 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercrand ccval = ccn 82 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercoct ccval = ccn 83 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnosc ccval = ccn 33 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnenv ccval = ccn 34 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsndec ccval = ccn 35 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnfilter ccval = ccn 36 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnres ccval = ccn 37 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatosc ccval = ccn 38 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatatk ccval = ccn 39 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatenv ccval = ccn 40 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatdec ccval = ccn 41 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatfilter ccval = ccn 42 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatres ccval = ccn 43 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkosc ccval = ccn 44 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkshp ccval = ccn 45 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkpch ccval = ccn 46 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkatk ccval = ccn 47 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkenv ccval = ccn 48 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkdec ccval = ccn 49 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nklofi ccval = ccn 50 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshapefilter ccval = ccn 53 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshaperes ccval = ccn 54 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nshapeamt ccval = ccn 55 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ncutoff ccval = ccn 56 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nwarm ccval = ccn 57 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ndistort ccval = ccn 58 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nres ccval = ccn 59 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nlfo1 ccval = ccn 114 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nlfo2 ccval = ccn 115 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    ntone ccval = ccn 116 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npan ccval = ccn 118 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nkvol ccval = ccn 12 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nsnvol ccval = ccn 13 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nhatvol ccval = ccn 14 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    npercvol ccval = ccn 15 #ccv (scale 0 127 $ ccval) #midichan 4 #mcc
    nbvar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 6
    nbassfilter ccval = ccn 50 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassres ccval = ccn 51 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassdecay ccval = ccn 52 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassenv ccval = ccn 53 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassaccent ccval = ccn 54 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassamp ccval = ccn 55 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassfreqshift ccval = ccn 56 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassmute ccval = ccn 59 #ccv (scale 0 127 $ ccval)  #mcc #midichan 6
    nbassoct ccval = ccn 60 #ccv ccval #mcc #midichan 6
    nbasslen ccval = ccn 80 #ccv ((ccval-1) * 8) #mcc #midichan 6
    nbassvol ccval = ccn 83 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nbassrandseq ccval = ccn 58 #ccv (scale 0 127 $ ccval) #mcc #midichan 6
    nvoicevar ccval = ccn 70 #ccv (ccval * 15) #mcc #midichan 7
    nvoicefilt1 ccval = ccn 102 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicefilt2 ccval = ccn 103 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicesp1 ccval = ccn 104 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicesp2 ccval = ccn 105 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicefb1 ccval = ccn 106 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicefb2 ccval = ccn 107 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoiceatk ccval = ccn 108 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicedec ccval = ccn 109 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicesus ccval = ccn 110 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicerel ccval = ccn 111 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoiceosc1 ccval = ccn 112 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoiceosc2 ccval = ccn 113 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoiceosc3 ccval = ccn 114 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicelfo1 ccval = ccn 115 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicelfo2 ccval = ccn 116 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    nvoicemod ccval = ccn 117 #ccv (scale 0 127 $ ccval) #midichan 7 #mcc
    