import {
    observable,
    action,
    computed
} from 'mobx';
import request from '../utils/request';
import io from 'socket.io-client';
import _ from 'lodash';

class controllerStore {

    sc_log_socket = io('http://localhost:4002/');
    snapshot_socket = io('http://localhost:4040/');
    @observable value = 20;
    @observable scmsg;
    @observable activeNordModularPerf = 'tidalnord_4';
    @observable activeOrbitIndex;
    @observable MIDIParams = [{}]
    @observable MIDIPresets = [{}]
    @observable activePreset = 'init'
    @observable presetName;
    controllerSnapshot = []
    @observable OrbitParams = [
        {
        name: 'speed',
        ccv: 0
        },
        {
        name: 'lpf',
        ccv: 10000
        },
        {
        name: 'coarse',
        ccv: 0
        },
        {
        name: 'dub',
        ccv: 0
        },
    ]

    
    constructor(){
        this.load()
        this.loadParams()
        let ctx = this;    
        this.snapshot_socket.on("/controllerSnapshot", action((data) => {
            // this.controllerSnapshot.push(data.msg)
            // console.log(this.controllerSnapshot)
               this.updateParams(data);
        }))

        this.sc_log_socket.on("/sclog", action((data) => {

            
            ctx.sc = data.trigger;
            let keys = Object.keys(data.trigger);
            
            for(let i = 0; i <keys.length; i++){
            
                if(keys[i]  === 'ctlNum' ){
                    for(let j = 0; j <ctx.MIDIParams.length; j++){

                        let pval = parseInt(data.trigger['control']);

                        if( ctx.MIDIParams[j].midichan === data.trigger ['midichan'] && ctx.MIDIParams[j].ccn === data.trigger ['ctlNum']) {

                            ctx.MIDIParams[j].ccv = pval;

                        }
                    }
                }
            } 
        }))
    }

    @action updateParams(data){
        console.log(data.msg.channel)
        _.forEach(this.MIDIParams, function(p) {
            if(data.msg.channel === parseInt(p.midichan ) && data.msg.controller === parseInt(p.ccn) ){
                p.ccv = data.msg.value
            }
        })
    }

    @action
    updatePerf(perf){
        this.activeNordModularPerf = perf;
        
    }
    @action
    updatePresetName(name){
        this.activePreset= name
    }
    @action
    compilePerf(){
        this.load();
        let perf = `:script /Users/canince/Documents/git/Siren/config/extras/tidal/` + this.activeNordModularPerf.toString() + '.hs' 
        this.submitGHC(perf);
    }

    @computed get activePerf (){
        return this.activeNordModularPerf;
    }



    @computed get getActivePreset (){
        return this.activePreset;
    }

    @computed get getActiveOrbitIndex(){
        return this.activeOrbitIndex;
    }

    @action updateOrbit(orbit){
        this.activeOrbitIndex = orbit;
    }
    
    @action changeSpeed(speed){
        this.OrbitParams[0].ccv = speed;
        let scChan = this.getActiveOrbitIndex + 1;
        let expr = "~d"+scChan+".set(\\speed," + speed + ");"
        this.submitSC(expr);

    }

    @action changeLPF(lpf){
        this.OrbitParams[1].ccv = lpf;
        let scChan = this.getActiveOrbitIndex + 1;
        let expr = "~d"+scChan+".set(\\lpf," + lpf + ");"
        this.submitSC(expr);
    }


    submitSC(expression) {
        request.post('http://localhost:3001/console_sc', {
            'pattern': expression
          })
          .then((response) => {
            console.log("RESPONSE SC", response);
          }).catch(function (error) {
            console.error("ERROR", error);
          });
      }

    
      submitGHC(expression) {
        request.post('http://localhost:3001/console_ghc', {
            'pattern': expression
          })
          .then((response) => {
            console.log("RESPONSE GHC");
          }).catch(function (error) {
            console.error("ERROR", error);
          });
      }

    @action
    modulateParam(name,val){
        let ctx = this;
        _.forEach(this.MIDIParams, function(p) {
            if(p.name === name.toString()){
                p.ccv = val;
                ctx.sendModulation(p.ccn, p.ccv, p.midichan)
            }
        });        
    }
    
    @action 
    sendModulation(ccn,ccv, midichan) { 
        //get cc number here
        let MIDIObj = {ccn :  parseInt(ccn) , ccv :  parseInt(ccv) , midichan: parseInt(midichan)}
        request.post('http://localhost:3001/MIDI', {
            'MIDIObj': MIDIObj
        })
        .then(action((response) => {
            if (response.status === 200) {
                console.log(response.status);
                
            } else {
                console.log("MIDI failed", response.status);
            }
        })).catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }));
    }
    
    getControllerSnapshot(){
        request.get('http://localhost:3001/nord', {})
        .catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }))
    }
    
    load() {
        request.post('http://localhost:3001/MIDIParams', {
            'activeNordModularPerf' : this.activeNordModularPerf
        })
        .then(action((response) => {
            if (response.data.MIDIObjArray) {
            this.MIDIParams = _.drop(response.data.MIDIObjArray,2);
            console.log(" ## MIDIParams loaded: ", response);
            }
        })).catch(function (error) {
            console.error(" ## MIDIParams errors: ", error);
        });
    };

    @action
    changePreset(name){
        let ctx = this;
        console.log("change preset")
       let pobj = this.MIDIPresets.find((o, i) => {
            
            if (o.presetName === name) {
                this.MIDIParams = o.MIDIParams
                this.activePreset = o.presetName 
                return true; // stop searching
            }  
        })
        if(pobj){
            _.forEach(this.MIDIParams, function(p) {
                ctx.sendModulation(p.ccn, p.ccv, p.midichan)
              
            })
        }  
    }

    @action
    deletePreset(name){
        console.log(name)
        let newMIDIArray = _.filter(this.MIDIPresets, function(o) {
            return o.presetName !== name
        });
        this.MIDIPresets = newMIDIArray
    }


    @action
    overwritePreset(name){
        let ctx = this;
        let midiObj= this.MIDIPresets ;
        
        let k = this.MIDIPresets.find((o, i) => {
            
            if (o.presetName === name) {
                midiObj[i] = this.MIDIParams
                return true; // stop searching
            }  
        })
        if(k){
            this.MIDIPresets = midiObj
        }
      
    }

    @action
    loadParams(){
        request.get('http://localhost:3001/MIDILoad')
        .then(action((response) => {
            if (response.data.MIDIPresets !== undefined) {
                this.MIDIPresets = response.data.MIDIPresets;
                console.log(" ## MIDI PRESETS loaded: ", this.MIDIPresets);
            }
        })).catch(function (error) {
            console.error(" ## GlobalStore errors: ", error);
        });
    }
    @action
    saveParams() {
        console.log("SAVING..");
        //TODO check for various validations
        let preset = {'presetName': this.getActivePreset, 'MIDIParams' :this.MIDIParams};
        this.MIDIPresets.push(preset)
    
        
        request.post('http://localhost:3001/MIDISave', {
            'MIDIPresets' : this.MIDIPresets
        })
        .then(action((response) => {
            if (response.status === 200 && response.data !== undefined) {
                console.log("MIDI PRESETS SAVED..")
            }
        })).catch(action((error) => {
            console.error(" ## Server errors: ", error);
        }));
    }



}
export default new controllerStore();